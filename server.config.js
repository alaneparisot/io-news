const serverConfig = {
  bcrypt: {
    salt: 10
  },
  jwt: {
    secretOrPrivateKey: 'secret'
  },
  mongodb: {
    url: 'localhost:27017/io-news'
  }
};

module.exports = serverConfig;
