const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const config = require('../../server.config');

const User = require('../models/user');

const oneWeek = 604800; // Seconds

router.post('/login', (req, res) => {
  User
    .findOne({email: req.body.email})
    .populate({
      path: 'bookmarks',
      populate: {
        path: 'item',
        component: 'Item'
      }
    })
    .exec((err, user) => {
      if (err) {
        return res.status(500).json({
          title: 'Something went wrong while logging in...',
          error: err
        });
      }
      if (!user) {
        return res.status(401).json({
          title: 'Something went wrong while logging in...',
          error: {message: 'E-mail address or password or both are incorrect.'}
        });
      }
      if (!bcrypt.compareSync(req.body.password, user.password)) {
        return res.status(401).json({
          title: 'Something went wrong while logging in...',
          error: {message: 'E-mail address or password or both are incorrect.'}
        });
      }

      const token = jwt.sign({user: user}, config.jwt.secretOrPrivateKey, {expiresIn: oneWeek});

      res.status(200).json({
        message: 'Successfully logged in!',
        token: token,
        user: user
      });
    });
});

router.post('/register', (req, res) => {
  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: bcrypt.hashSync(req.body.password, config.bcrypt.salt),
    email: req.body.email,
  });
  user.save((err, result) => {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while registering user...',
        error: err
      });
    }

    const token = jwt.sign({user: result}, config.jwt.secretOrPrivateKey, {expiresIn: oneWeek});

    res.status(201).json({
      message: 'User was successfully registered!',
      token: token,
      user: user
    });
  });
});

// Route Protection
router.use('/', (req, res, next) => {
  jwt.verify(req.query.token, config.jwt.secretOrPrivateKey, (err, decodedToken) => {
    if (err) {
      return res.status(401).json({
        title: 'Being logged in is required to execute this action.',
        error: err
      });
    }
    next();
  });
});

router.get('/', (req, res) => {
  const decodedToken = jwt.decode(req.query.token);
  User
    .findById(decodedToken.user._id)
    .populate({
      path: 'bookmarks',
      populate: {
        path: 'item',
        component: 'Item'
      }
    })
    .exec((err, user) => {
      if (err) {
        return res.status(500).json({
          title: `Something went wrong while looking for user ${decodedToken.user._id}...`,
          error: err
        });
      }
      res.status(201).json({
        message: 'User has been found.',
        obj: {user: user}
      });
    });
});

module.exports = router;
