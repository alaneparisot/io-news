const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const config = require('../../server.config');

const Bookmark = require('../models/bookmark');
const Item = require('../models/item');
const User = require('../models/user');

// Route Protection
router.use('/', (req, res, next) => {
  jwt.verify(req.query.token, config.jwt.secretOrPrivateKey, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        title: 'Being logged in is required to execute this action.',
        error: err
      });
    }
    next();
  });
});

router.post('/', (req, res) => {
  const decoded = jwt.decode(req.query.token);
  User
    .findById(decoded.user._id)
    .populate({
      path: 'bookmarks',
      populate: {
        path: 'item',
        component: 'Item'
      }
    })
    .exec((err, user) => {
      if (err || !user) {
        return res.status(500).json({
          title: 'Something went wrong while bookmarking...',
          error: err
        });
      }
      Item.findById(req.body.itemId, (errItem, item) => {
        if (errItem) {
          return res.status(500).json({
            title: `Something went wrong while looking for item ${req.body.itemId}...`,
            error: errItem
          });
        }
        if (!item) {
          return res.status(404).json({
            title: `Item not found...`,
            error: {message: `Item ${req.params.id} was not found.`}
          });
        }
        const newBookmark = new Bookmark({
          item: item,
          creationDate: Date.now()
        });
        newBookmark.save((errBookmark, savedBookmark) => {
          if (errBookmark || !savedBookmark) {
            return res.status(500).json({
              title: 'Something went wrong while bookmarking...',
              error: err
            });
          }
          user.bookmarks.push(savedBookmark);
          user.save();
          res.status(201).json({
            message: 'Item was successfully bookmarked.',
            obj: {user: user}
          });
        });
      });
    });
});

router.delete('/:id', (req, res) => {
  const decoded = jwt.decode(req.query.token);
  User
    .findById(decoded.user._id)
    .populate({
      path: 'bookmarks',
      populate: {
        path: 'item',
        component: 'Item'
      }
    })
    .exec((err, user) => {
      if (err || !user) {
        return res.status(500).json({
          title: 'Something went wrong while deleting bookmark...',
          error: err
        });
      }
      user.bookmarks.pull(req.params.id);
      user.save();
      res.status(200).json({
        message: 'Bookmark was successfully deleted!',
        obj: {user: user}
      });
    });
});

module.exports = router;
