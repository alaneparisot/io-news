const express = require('express');
const router = express.Router();

const List = require('../models/list');

router.get('/:id', function (req, res) {
  List
    .findById({_id: req.params.id})
    .populate('items')
    .exec(function (err, list) {
      if (err) {
        return res.status(500).json({
          title: `Something went wrong while looking for list ${req.params.id}...`,
          error: err
        });
      }

      if (!list) {
        return res.status(404).json({
          title: `List not found...`,
          error: {message: `List ${req.params.id} was not found.`}
        });
      }

      // Create an array with all the items' scores of the list, in descending order
      // TODO: Find a way to do this before and once (e.g., when the list is updated)
      list.scores = [];
      for (const item of list.items) {
        list.scores.push(item.score);
      }
      list.scores.sort(function (a, b) {
        return a - b;
      });

      res.status(200).json({
        message: `List ${req.params.id} were successfully fetched!`,
        obj: list
      });
    });
});

module.exports = router;
