const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const schema = new Schema({
  _id: {type: String, required: true, unique: true},
  items: [{type: Number, ref: 'Item'}],
  scores: [Number]
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('List', schema);
