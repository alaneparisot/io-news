const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const schema = new Schema({
  _id: {type: Number, required: true, unique: true},
  deleted: Boolean,
  type: {type: String, required: true},
  by: {type: String, required: true}, // Associate to User
  time: {type: Number, required: true},
  text: String,
  dead: Boolean,
  parent: {type: Number, ref: 'Item'},
  poll: {type: Number, ref: 'Item'},
  kids: [{type: Number, ref: 'Item'}],
  url: String,
  score: Number,
  title: String,
  parts: [{type: Number, ref: 'Item'}],
  descendants: Number
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Item', schema);
