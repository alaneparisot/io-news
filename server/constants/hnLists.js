const hnLists = [
  {
    name: 'newstories',
    interval: 900000,
    timeout: 0
  },
  {
    name: 'topstories',
    interval: 1800000,
    timeout: 30000
  },
  {
    name: 'beststories',
    interval: 3600000,
    timeout: 60000
  },
  {
    name: 'askstories',
    interval: 7200000,
    timeout: 90000
  },
  {
    name: 'jobstories',
    interval: 7200000,
    timeout: 90000
  },
  {
    name: 'showstories',
    interval: 7200000,
    timeout: 90000
  }
];

module.exports = hnLists;
