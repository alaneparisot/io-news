const https = require('https');

const Item = require('../models/item');
const List = require('../models/list');

class Lists {

  constructor() { }

  static watch(hnLists) {
    const host = 'hacker-news.firebaseio.com';

    // 1/4) For each list...
    for (let hnList of hnLists) {

      function execute() {

        // 2/4) ...fetch the items currently in...
        https.get({
          host: host,
          path: `/v0/${hnList.name}.json`
        }, function (resList) {
          resList.on('data', function (dataList) {
            const itemIds = JSON.parse(dataList);

            if (!itemIds) { return; }

            // 3/4) ...save or update the list with the fetched items...
            List.findOne({_id: hnList.name}, function (errFindList, foundList) {
              if (errFindList) {
                return console.error(`Something went wrong while looking for list "${hnList.name}"...`, errFindList);
              }
              if (!foundList) {
                // Create
                const newList = new List({
                  _id: hnList.name,
                  items: itemIds
                });
                return newList.save(function (errSaveList, createdList) {
                  if (errSaveList) {
                    return console.error(`Something went wrong while creating list "${newList._id}"...`, errSaveList);
                  }
                });
              }
              // Update
              foundList.items = itemIds;
              foundList.save(function (errUpdateList, updatedList) {
                console.info(`${hnList.name} last update: ${(new Date()).toUTCString()}`);
                if (errUpdateList) {
                  return console.error(`Something went wrong while updating list "${foundList._id}"...`, errUpdate);
                }
              });
            });

            // 4/4) ...and save or update each item.
            for (const itemId of itemIds) {
              https.get({
                host: host,
                path: `/v0/item/${itemId}.json`
              }, function (resItem) {
                resItem.on('data', function (dataItem) {
                  const item = JSON.parse(dataItem);

                  if (!(item && typeof item.id === 'number')) { return; }

                  Item.findOne({_id: item.id}, function (errFind, foundItem) {
                    if (errFind) {
                      return console.error(`Something went wrong while looking for item ${item.id}...`, errFind);
                    }
                    if (!foundItem) {
                      // Create
                      item._id = item.id;
                      const newItem = new Item(item);
                      return newItem.save(function (errSave, createdItem) {
                        if (errSave) {
                          return console.error(`Something went wrong while creating item ${newItem._id}...`, errSave);
                        }
                      });
                    }
                    // Update
                    Object.assign(foundItem, item);
                    foundItem.save(function (errUpdate, updatedItem) {
                      if (errUpdate) {
                        return console.error(`Something went wrong while updating item ${foundItem._id}...`, errUpdate);
                      }
                    });
                  });
                });
              }).on('error', (errGetItem) => {
                console.error(`Something went wrong while getting item ${itemId}...`, errGetItem);
              });
            }
          });
        }).on('error', (errGetList) => {
          console.error(`Something went wrong while getting list ${hnList.name}...`, errGetList);
        });
      }

      setTimeout(execute, hnList.timeout);
      setInterval(execute, hnList.interval);
    }
  }
}

module.exports = Lists;
