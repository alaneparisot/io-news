import {
  async, ComponentFixture, inject,
  TestBed
} from '@angular/core/testing';

import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BookmarkService } from '../../user/bookmarks/service/bookmark.service';
import { Item } from '../../shared/items/model/item.model';
import { ItemService } from '../../shared/items/services/item/item.service';
import { ListService } from '../../shared/items/services/list/list.service';
import { StoryListComponent } from './story-list.component';
import { UserService } from '../../user/service/user.service';
import { AuthService } from '../../user/auth/service/auth.service';

describe('StoryListComponent', () => {
  let component: StoryListComponent;
  let fixture: ComponentFixture<StoryListComponent>;

  let authService: AuthService;
  let bookmarkService: BookmarkService;
  let itemService: ItemService;
  let listService: ListService;
  let userService: UserService;

  class MockRouter {
    navigateByUrl(url: string) { return url; }
  }

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [
          HttpModule,
          NgbModule.forRoot()
        ],
        declarations: [StoryListComponent],
        providers: [
          AuthService,
          BookmarkService,
          ItemService,
          ListService,
          UserService,
          {provide: Router, useClass: MockRouter}
        ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryListComponent);
    component = fixture.componentInstance;

    authService = fixture.debugElement.injector.get(AuthService);
    bookmarkService = fixture.debugElement.injector.get(BookmarkService);
    itemService = fixture.debugElement.injector.get(ItemService);
    listService = fixture.debugElement.injector.get(ListService);
    userService = fixture.debugElement.injector.get(UserService);

    spyOn(listService, 'loadList');
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });


  describe('ngOnInit', () => {

    it('should load list', () => {
      fixture.detectChanges();
      expect(listService.loadList).toHaveBeenCalled();
    });

  });


  describe('getCurrentList', () => {

    it('should get current list', () => {
      const item1 = new Item({});
      const item2 = new Item({});
      spyOn(listService, 'getCurrentList').and.returnValue([item1, item2]);
      expect(component.getCurrentList()).toEqual([item1, item2]);
    });

  });


  describe('getCurrentListId', () => {

    it('should get current list id', () => {
      spyOn(listService, 'getCurrentListId').and.returnValue('teststories');
      expect(component.getCurrentListId()).toBe('teststories');
    });

  });


  describe('getItemHnUrl', () => {

    it('should get item HN URL', () => {
      spyOn(itemService, 'getItemHnUrl')
        .and.returnValue('https://news.ycombinator.com/item?id=123');
      expect(component.getItemHnUrl(123))
        .toBe('https://news.ycombinator.com/item?id=123');
    });

  });


  describe('getItemWebsite', () => {

    it('should get item website', () => {
      spyOn(itemService, 'getItemWebsite')
        .and.returnValue('https://www.test.com');
      expect(component.getItemWebsite(
        'https://www.test.com/article.html',
        false
      )).toBe('https://www.test.com');
    });

  });


  describe('getStoryRankingStyle', () => {

    beforeEach(() => {
      spyOn(itemService, 'getItemRank').and.returnValue(1);
      fixture.detectChanges();
      component.rankingColors = ['#aaa', '#bbb', '#ccc'];
    });

    it('should get item row background color', () => {
      component.sortingProperty = 'none';
      expect(component.getStoryRankingStyle(123)).toEqual({
        backgroundColor: '#bbb'
      });
    });

    it('should get item row bottom border style', () => {
      component.sortingProperty = 'score';
      expect(component.getStoryRankingStyle(123)).toEqual({
        borderBottom: '1px solid #bbb'
      });
    });

  });


  describe('isBookmarked', () => {

    it('should return true', () => {
      spyOn(bookmarkService, 'isBookmarked').and.returnValue(true);
      expect(component.isBookmarked(1)).toBeTruthy();
    });

    it('should return false', () => {
      spyOn(bookmarkService, 'isBookmarked').and.returnValue(false);
      expect(component.isBookmarked(1)).toBeFalsy();
    });

  });


  describe('isListSorted', () => {

    it('should return true', () => {
      // fixture.detectChanges();
      component.sortingProperty = 'score';
      expect(component.isListSorted()).toBeTruthy();
      expect(component.isListSorted('score')).toBeTruthy();
    });

    it('should return false', () => {
      fixture.detectChanges();
      component.sortingProperty = 'none';
      expect(component.isListSorted()).toBeFalsy();
      expect(component.isListSorted('descendants')).toBeFalsy();
    });

  });


  describe('onBookmark', () => {

    it('should add to bookmarks', () => {
      spyOn(bookmarkService, 'isBookmarked').and.returnValue(false);
      spyOn(bookmarkService, 'addBookmark').and.returnValue(Observable.of({}));
      component.onBookmark(1);
      expect(bookmarkService.addBookmark).toHaveBeenCalledWith(1);
    });

    it('should delete from bookmarks', () => {
      spyOn(bookmarkService, 'isBookmarked').and.returnValue(true);
      spyOn(bookmarkService, 'deleteBookmarkFromItemId')
        .and.returnValue(Observable.of({}));
      component.onBookmark(1);
      expect(bookmarkService.deleteBookmarkFromItemId).toHaveBeenCalledWith(1);
    });

    describe('redirection', () => {

      let router: Router;

      beforeEach(inject([Router], (_router: Router) => {
        router = _router;
        spyOn(router, 'navigateByUrl');
      }));

      it('should go to login page', () => {
        spyOn(authService, 'isAuthenticated').and.returnValue(false);
        component.onBookmark(123);
        expect(router.navigateByUrl).toHaveBeenCalledWith('user/login');
      });

      it('should not go to login page', () => {
        spyOn(authService, 'isAuthenticated').and.returnValue(true);
        spyOn(component, 'isBookmarked').and.returnValue(false);
        spyOn(bookmarkService, 'addBookmark').and.returnValue(Observable.of());
        component.onBookmark(123);
        expect(router.navigateByUrl).not.toHaveBeenCalled();
      });

    });

  });


  describe('setSortingProperty', () => {

    it('should set sorting property', () => {
      component.setSortingProperty('score');
      expect(component.sortingProperty).toBe('score');
    });

    it('should reset sorting property', () => {
      component.sortingProperty = 'score';
      component.setSortingProperty('score');
      expect(component.sortingProperty).toBe('none');
    });

  });

});
