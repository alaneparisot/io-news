import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../user/auth/service/auth.service';
import { BookmarkService } from '../../user/bookmarks/service/bookmark.service';
import { Item } from '../../shared/items/model/item.model';
import { ItemService } from '../../shared/items/services/item/item.service';
import { ListService } from '../../shared/items/services/list/list.service';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.scss']
})
export class StoryListComponent implements OnInit {
  rankingColors: string[];
  sortingProperty: string;

  constructor(private authService: AuthService,
              private bookmarkService: BookmarkService,
              private itemService: ItemService,
              private listService: ListService,
              private router: Router) { }

  ngOnInit(): void {
    this.listService.loadList();

    // TODO: Use CSS classes and SCSS variables (see src/styles.scss)
    this.rankingColors = ['#f8d7da', '#fff3cd', '#d4edda'];

    this.sortingProperty = localStorage.getItem('sortingProperty') || 'none';
  }

  getCurrentList(): Item[] {
    return this.listService.getCurrentList(this.sortingProperty);
  }

  getCurrentListId(): string {
    return this.listService.getCurrentListId();
  }

  getItemHnUrl(itemId: number): string {
    return this.itemService.getItemHnUrl(itemId);
  }

  getItemWebsite(url: string, hnAsUrl: boolean): string {
    return this.itemService.getItemWebsite(url, hnAsUrl);
  }

  /**
   * Return Style of Story Row: Background for Unsorted List, Border Otherwise
   * @param {number} itemScore Item's Score
   * @returns {{backgroundColor: string} | {borderBottom: string}}
   */
  getStoryRankingStyle(itemScore: number): { backgroundColor: string } |
    { borderBottom: string } {
    const color = this.rankingColors[this.itemService.getItemRank(itemScore)];

    if (this.isListSorted()) {
      return {borderBottom: `1px solid ${color || '#e9ecef'}`};
    }

    return {backgroundColor: color};
  }

  isBookmarked(itemId: number): boolean {
    return this.bookmarkService.isBookmarked(itemId);
  }

  /**
   * Return if the List is Sorted
   * @param {string} sortingProperty Will return true if this is the property
   *    by which the list is sorted (optional)
   * @returns {boolean}
   */
  isListSorted(sortingProperty?: string): boolean {
    if (!this.sortingProperty || this.sortingProperty === 'none') {
      return false;
    } else if (!sortingProperty) {
      return true;
    } else {
      return sortingProperty === this.sortingProperty;
    }
  }

  onBookmark(itemId: number): void {
    if (!this.authService.isAuthenticated()) {
      this.router.navigateByUrl('user/login');
      return;
    }

    if (this.isBookmarked(itemId)) {
      this.bookmarkService.deleteBookmarkFromItemId(itemId)
        .subscribe(
          // data => console.log('deleteBookmark:success', data), // TODO
          // error => console.error('deleteBookmark:error', error) // TODO
        );
    } else {
      this.bookmarkService.addBookmark(itemId)
        .subscribe(
          // data => console.log('addBookmark:success', data), // TODO
          // error => console.error('addBookmark:error', error) // TODO
        );
    }
  }

  /**
   * Set or Reset Sorting Property if Argument is Already the Current One
   * @param {string} sortingProperty Sorting property
   */
  setSortingProperty(sortingProperty: string): void {
    this.sortingProperty = this.sortingProperty !== sortingProperty
      ? sortingProperty
      : 'none';
  }

}
