import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from './home.component';
import { HeaderComponent } from '../shared/header/header.component';
import { StoryListComponent } from './story-list/story-list.component';
import { ShortenPipe } from '../shared/header/pipes/shorten.pipe';
import { ItemService } from '../shared/items/services/item/item.service';
import { ListService } from '../shared/items/services/list/list.service';
import { BookmarkService } from '../user/bookmarks/service/bookmark.service';
import { UserService } from '../user/service/user.service';
import { AuthService } from '../user/auth/service/auth.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [
          RouterTestingModule,
          HttpModule,
          NgbModule.forRoot()
        ],
        declarations: [
          HomeComponent,
          HeaderComponent,
          StoryListComponent,
          ShortenPipe
        ],
        providers: [
          AuthService,
          BookmarkService,
          ItemService,
          ListService,
          UserService
        ],
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
