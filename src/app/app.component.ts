import { Component, OnInit } from '@angular/core';

import { AuthService } from './user/auth/service/auth.service';
import { UserService } from './user/service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private authService: AuthService,
              private userService: UserService) {}

  ngOnInit() {
    if (localStorage.getItem('userId') && localStorage.getItem('token')) {
      this.userService.getUserFromToken()
        .subscribe(
          data => null, // TODO
          error => this.authService.logout(true)
        );
    }
  }
}
