import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { ShortenPipe } from './shared/header/pipes/shorten.pipe';
import { AuthService } from './user/auth/service/auth.service';
import { ListService } from './shared/items/services/list/list.service';
import { UserService } from './user/service/user.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let authService: AuthService;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [RouterTestingModule, HttpModule],
        declarations: [AppComponent, HeaderComponent, ShortenPipe],
        providers: [AuthService, ListService, UserService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;

    authService = fixture.debugElement.injector.get(AuthService);
    userService = fixture.debugElement.injector.get(UserService);
  });

  it('should create the app', async(() => {
    expect(component).toBeDefined();
  }));


  describe('ngOnInit', () => {

    beforeEach(() => {
      spyOn(userService, 'getUserFromToken')
        .and.returnValue(Observable.throw({}));
    });

    it('should not call UserService.getUserFromToken', () => {
      window.localStorage.removeItem('userId');
      fixture.detectChanges();
      expect(userService.getUserFromToken).not.toHaveBeenCalled();
    });

    it('should call UserService.getUserFromToken', () => {
      window.localStorage.setItem('userId', 'userId');
      window.localStorage.setItem('token', 'token');
      spyOn(authService, 'logout');
      fixture.detectChanges();
      expect(userService.getUserFromToken).toHaveBeenCalled();
      expect(authService.logout).toHaveBeenCalledWith(true);
    });

  });

});
