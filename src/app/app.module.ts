import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemService } from './shared/items/services/item/item.service';
import { ListService } from './shared/items/services/list/list.service';
import { HeaderComponent } from './shared/header/header.component';
import { ShortenPipe } from './shared/header/pipes/shorten.pipe';
import { AuthService } from './user/auth/service/auth.service';
import { UserComponent } from './user/user.component';
import { BookmarkService } from './user/bookmarks/service/bookmark.service';
import { UserService } from './user/service/user.service';
import { HomeModule } from './home/home.module';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    HomeModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    ShortenPipe,
    UserComponent,
  ],
  providers: [
    AuthService,
    ItemService,
    ListService,
    BookmarkService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
