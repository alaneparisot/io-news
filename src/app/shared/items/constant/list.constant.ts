export const LIST = {
  allIds: [
    'newstories',
    'topstories',
    'beststories',
    'showstories',
    'askstories',
    'jobstories'
  ],
  defaultId: 'topstories'
};
