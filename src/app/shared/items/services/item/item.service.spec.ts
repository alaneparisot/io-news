import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { ItemService } from './item.service';
import { ListService } from '../list/list.service';

describe('ItemService', () => {
  let service: ItemService;
  let listService: ListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [ItemService, ListService]
    });
  });

  beforeEach(inject(
    [ItemService, ListService],
    (itemService: ItemService, _listService: ListService) => {
      service = itemService;
      listService = _listService;
    }
  ));

  it('should be created', () => {
    expect(service).toBeDefined();
  });


  describe('getItemHnUrl', () => {

    it('should get item HN URL', () => {
      expect(service.getItemHnUrl(123))
        .toBe('https://news.ycombinator.com/item?id=123');
    });

  });


  describe('getItemRank', () => {

    beforeEach(() => {
      spyOn(listService, 'getCurrentListScores')
        .and.returnValue([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    });

    it('should get item rank < 50%', () => {
      expect(service.getItemRank(1)).toBeUndefined();
      expect(service.getItemRank(2)).toBeUndefined();
      expect(service.getItemRank(3)).toBeUndefined();
      expect(service.getItemRank(4)).toBeUndefined();
      expect(service.getItemRank(5)).toBeUndefined();
    });

    it('should get item rank >= 50% and < 66%', () => {
      expect(service.getItemRank(6)).toBe(2);
      expect(service.getItemRank(7)).toBe(2);
    });

    it('should get item rank >= 66% and < 90%', () => {
      expect(service.getItemRank(8)).toBe(1);
      expect(service.getItemRank(9)).toBe(1);
    });

    it('should get item rank >= 90%', () => {
      expect(service.getItemRank(10)).toBe(0);
    });

  });


  describe('getItemWebsite', () => {

    it('should return HN as URL', () => {
      expect(service.getItemWebsite(null, true)).toBe('news.ycombinator.com');
    });

    it('should return HN as name', () => {
      expect(service.getItemWebsite(null)).toBe('Hacker News');
    });

    it('should handle http and https', () => {
      expect(service.getItemWebsite('http://www.techcrunch.com/article'))
        .toBe('techcrunch.com');
      expect(service.getItemWebsite('https://www.techcrunch.com/article'))
        .toBe('techcrunch.com');
    });

    it('should handle sub domains', () => {
      expect(service.getItemWebsite('http://author.blog.techcrunch.com/article'))
        .toBe('author.blog.techcrunch.com');
    });

  });

});
