import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';

import { ListService } from '../list/list.service';

@Injectable()
export class ItemService {

  constructor(private listService: ListService) { }

  /**
   * Return the Hacker News URL of an Item (Where Comments Are Displayed)
   * @param {number} itemId The item's ID
   * @returns {string} The Hacker News URL of the item
   */
  getItemHnUrl(itemId: number): string {
    return `https://news.ycombinator.com/item?id=${itemId}`;
  }

  /**
   * Return the Rank of an Item, based on its score
   * @param {number} itemScore The item's score
   * @returns {number} The item's ranking (the lower the higher, i.e., 0 = high)
   */
  getItemRank(itemScore: number): number {
    const currentListScores = this.listService.getCurrentListScores();
    const currentListLength = currentListScores.length;
    const itemScoreIndex = currentListScores.indexOf(itemScore);

    if (itemScoreIndex >= (currentListLength * .9)) {
      return 0;
    } else if (itemScoreIndex >= (currentListLength * .66)) {
      return 1;
    } else if (itemScoreIndex >= (currentListLength * .5)) {
      return 2;
    }
  }

  /**
   * Return the Domain Name of an URL
   * @param {string} url The URL to parse, e.g., 'http://www.example.com/foo.html'
   * @param {boolean} hnAsUrl True will return Hacker News as an URL (optional)
   * @returns {string} The extracted domain name, e.g., 'example.com'
   */
  getItemWebsite(url: string, hnAsUrl?: boolean): string {
    // TODO: Do this in the backend, when the Item is created.
    // TODO: Return an object instead, with label and URL.
    if (!isNullOrUndefined(url)) {
      return url.match(/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im)[1];
    }

    return hnAsUrl ? 'news.ycombinator.com' : 'Hacker News';
  }

}
