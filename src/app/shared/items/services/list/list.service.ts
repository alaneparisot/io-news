import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { isNullOrUndefined } from 'util';

import { environment } from '../../../../../environments/environment';

import { LIST } from '../../constant/list.constant';
import { Item } from '../../model/item.model';

@Injectable()
export class ListService {
  private currentList: Item[];
  private currentListId: string;
  private currentListScores: number[];
  private listIds: string[];

  constructor(private http: Http) {
    this.listIds = LIST.allIds.slice();
    this.currentListId = LIST.defaultId + '';
  }

  /**
   * Return the Current List
   * @param {string} sortingProperty Item's property by which the list should be sorted, e.g., 'score'
   * @returns {Item[]} List of Items
   */
  getCurrentList(sortingProperty?: string): Item[] {

    // Store (or Remove) the Sorting Property into Local Storage
    if (!isNullOrUndefined(sortingProperty)) {
      if (sortingProperty === 'none') {
        localStorage.removeItem('sortingProperty');
      } else {
        localStorage.setItem('sortingProperty', sortingProperty);
      }
    }

    // Check If Sorting Must Be Done on the List
    const hasCurrentList = !isNullOrUndefined(this.currentList);
    const hasSortingProperty = !isNullOrUndefined(localStorage.getItem('sortingProperty'));
    if (hasCurrentList && hasSortingProperty) {
      // Return a Sorted List
      return this.sortList(this.currentList.slice(), localStorage.getItem('sortingProperty'));
    }

    // Return an Unsorted List
    return this.currentList;
  }

  /**
   * Return the Current List's Identifier, e.g., 'topstories'
   * @returns {string} List Identifier
   */
  getCurrentListId(): string {
    return this.currentListId;
  }

  /**
   * Return the Current List's Scores
   * @returns {number[]} List of Scores
   */
  getCurrentListScores(): number[] {
    return this.currentListScores;
  }

  /**
   * Return the List of All List IDs
   * @returns {string[]}
   */
  getListIds(): string[] {
    return this.listIds;
  }

  /**
   * Fetch List's Items from Database and Store Them
   * @param {string} listId List's identifier, e.g., 'topstories'
   */
  loadList(listId?: string): void {
    if (!isNullOrUndefined(listId)) {
      localStorage.setItem('listId', listId);
    }

    this.currentListId = localStorage.getItem('listId') || this.currentListId;

    this.http.get(`${environment.host}/api/list/${this.currentListId}`)
      .map((response: Response) => {
        return response.json().obj;
      })
      .catch((error: Response) => Observable.throw(error.json()))
      .subscribe((list) => {
        if (isNullOrUndefined(list)) { return; }

        const newList = [];

        // Populate the Current List
        for (const item of list.items) {
          newList.push(new Item(item));
        }
        this.currentList = newList;
        this.currentListScores = list.scores;
      });
  }

  // Private Methods -----------------------------------------------------------

  /**
   * Sort a List of Items Accordingly to a Specific Property
   * @param {Item[]} list The list to sort
   * @param {string} sortingProperty The property to sort the list with
   * @returns {Item[]} A sorted list of items
   */
  private sortList(list: Item[], sortingProperty: string): Item[] {
    return list.sort((a, b) => {
      const valueA = a[sortingProperty];
      const valueB = b[sortingProperty];

      if (isNullOrUndefined(valueA)) { return 1; }
      if (isNullOrUndefined(valueB)) { return -1; }

      // TODO: Compare another property if those are equal
      return valueB - valueA;
    });
  }

}
