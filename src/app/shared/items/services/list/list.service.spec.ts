import { TestBed, inject } from '@angular/core/testing';
import {
  HttpModule, Response, ResponseOptions,
  XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { LIST } from '../../constant/list.constant';
import { ListService } from './list.service';
import { Item } from '../../model/item.model';

describe('ListService', () => {
  let service: ListService;

  const mockResponse = {
    obj: {
      items: [
        {id: 1, descendants: 1, score: 2},
        {id: 2, score: 3},
        {id: 3, descendants: 2}
      ],
      scores: [2, 3]
    }
  };

  LIST.allIds = ['teststories1', 'teststories2', 'teststories3'];
  LIST.defaultId = 'teststories2';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [ListService, {provide: XHRBackend, useClass: MockBackend}]
    });
  });

  beforeEach(inject(
    [ListService, XHRBackend],
    (listService: ListService, mockBackend: MockBackend) => {
      service = listService;

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });

      service.loadList('teststories3');
    }));

  it('should be created', () => {
    expect(service).toBeDefined();
  });


  describe('getCurrentList', () => {

    it('should get the current list, unsorted', () => {
      expect(service.getCurrentList('none')).toEqual(
        [
          new Item({id: 1, descendants: 1, score: 2}),
          new Item({id: 2, score: 3}),
          new Item({id: 3, descendants: 2})
        ]
      );
    });

    it('should get the current list, sorted by score', () => {
      expect(service.getCurrentList('score')).toEqual(
        [
          new Item({id: 2, score: 3}),
          new Item({id: 1, descendants: 1, score: 2}),
          new Item({id: 3, descendants: 2})
        ]
      );
    });

    it('should get the current list, sorted by descendants', () => {
      expect(service.getCurrentList('descendants')).toEqual(
        [
          new Item({id: 3, descendants: 2}),
          new Item({id: 1, descendants: 1, score: 2}),
          new Item({id: 2, score: 3})
        ]
      );
    });

  });


  describe('getCurrentListId', () => {

    it('should get current list id', () => {
      expect(service.getCurrentListId()).toBe('teststories3');
    });

  });


  describe('getCurrentListScores', () => {

    it('should get current list scores', () => {
      expect(service.getCurrentListScores()).toEqual([2, 3]);
    });

  });


  describe('getListIds', () => {

    it('should get list ids', () => {
      expect(service.getListIds())
        .toEqual(['teststories1', 'teststories2', 'teststories3']);
    });

  });


  describe('loadList', () => {

    it('should set local storage with list id', () => {
      expect(window.localStorage.getItem('listId')).toBe('teststories3');
    });

  });

})
;
