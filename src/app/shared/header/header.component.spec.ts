import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpModule } from '@angular/http';

import { HeaderComponent } from './header.component';
import { ShortenPipe } from './pipes/shorten.pipe';
import { ListService } from '../items/services/list/list.service';
import { AuthService } from '../../user/auth/service/auth.service';
import { UserService } from '../../user/service/user.service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  let authService: AuthService;
  let listService: ListService;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [RouterTestingModule, HttpModule],
        declarations: [HeaderComponent, ShortenPipe],
        providers: [AuthService, ListService, UserService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;

    authService = fixture.debugElement.injector.get(AuthService);
    listService = fixture.debugElement.injector.get(ListService);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });


  describe('ngOnInit', () => {

    it('should get list ids', () => {
      spyOn(listService, 'getListIds')
        .and.returnValue(['teststories1', 'teststories2']);
      fixture.detectChanges();
      expect(component.listIds).toEqual(['teststories1', 'teststories2']);
    });

  });


  describe('isAuthRouteActive', () => {

    // TODO

  });


  describe('isCurrentListId', () => {

    it('should tell if arg is current list id', () => {
      spyOn(listService, 'getCurrentListId').and.returnValue('teststories');
      expect(component.isCurrentListId('teststories')).toBeTruthy();
      expect(component.isCurrentListId('otherstories')).toBeFalsy();
    });

  });


  describe('onListIdSelect', () => {

    it('should load list on list id select', fakeAsync(() => {
      spyOn(listService, 'loadList');
      component.onListIdSelect('teststories');
      expect(listService.loadList).toHaveBeenCalledWith('teststories');
    }));

  });

  describe('onLogout', () => {

      it('should call AuthService\'s logout method', () => {
        spyOn(authService, 'logout');
        component.onLogout();
        expect(authService.logout).toHaveBeenCalled();
      });

  });

});
