import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import { ListService } from '../items/services/list/list.service';
import { AuthService } from '../../user/auth/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {
  listIds: string[];

  constructor(private authService: AuthService,
              private config: NgbDropdownConfig,
              private listService: ListService,
              private router: Router) {
    config.placement = 'bottom-right';
  }

  ngOnInit(): void {
    this.listIds = this.listService.getListIds();
  }

  isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  isAuthRouteActive(): boolean {
    return this.router.isActive('auth', false);
  }

  isCurrentListId(listId: string): boolean {
    const isListRoute = this.router.isActive('', true);
    const isCurrentListId = listId === this.listService.getCurrentListId();
    return isListRoute && isCurrentListId;
  }

  onListIdSelect(selectedListId): void {
    this.listService.loadList(selectedListId);
  }

  onLogout(): void {
    this.authService.logout();
  }

}
