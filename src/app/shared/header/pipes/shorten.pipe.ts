import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  /**
   * Remove 'stories' from the List ID
   * @param {string} listId The list ID
   * @returns {string} The list ID minus 'stories'
   */
  transform(listId: string): string {
    return listId.substring(0, listId.indexOf('stories'));
  }

}
