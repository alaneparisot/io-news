import { ShortenPipe } from './shorten.pipe';

describe('ShortenPipe', () => {
  let pipe: ShortenPipe;

  beforeEach(() => {
    pipe = new ShortenPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeDefined();
  });


  describe('transform', () => {

    it('should transform "teststories" to "test"', () => {
      expect(pipe.transform('teststories')).toBe('test');
    });

  });

});
