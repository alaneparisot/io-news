import { TestBed, inject } from '@angular/core/testing';
import {
  HttpModule,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';

import { User } from '../model/user.model';
import { UserService } from './user.service';
import { MockBackend, MockConnection } from '@angular/http/testing';

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [UserService, {provide: XHRBackend, useClass: MockBackend}]
    });
  });

  beforeEach(inject([UserService], (userService: UserService) => {
    service = userService;
  }));

  it('should be created', () => {
    expect(service).toBeDefined();
  });


  describe('setUser + getUser', () => {

    it('should set user', () => {
      const user: User = new User({
        email: 'john@doe.com',
        password: 'secret'
      });
      spyOn(service.userIsUpdated, 'next');
      service.setUser(user);
      expect(service.getUser()).toBe(user);
      expect(service.userIsUpdated.next).toHaveBeenCalledWith(user);
    });

  });


  describe('getUserFromToken', () => {

    const user = new User({
      email: 'john.doe@getuserfromtoken.com',
      password: 'secret',
      firstName: 'John',
      lastName: 'Doe',
      bookmarks: []
    });

    const mockResponse = {
      obj: {
        user: user
      }
    };

    it('should update stored user', inject([XHRBackend],
      (mockBackend: MockBackend) => {

        mockBackend.connections.subscribe((connection: MockConnection) => {
          expect(connection.request.url).toMatch(/api\/user\?token=token-user/);
          connection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(mockResponse)
          })));
        });

        spyOn(service.userIsUpdated, 'next');

        window.localStorage.setItem('token', 'token-user');
        service.getUserFromToken().subscribe(() => {
          expect(service.userIsUpdated.next).toHaveBeenCalledWith(jasmine.any(User));
          expect(service.getUser().email).toBe('john.doe@getuserfromtoken.com');
        });
      })
    );

  });

});
