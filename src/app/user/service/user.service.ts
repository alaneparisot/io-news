import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';
import { User } from '../model/user.model';

@Injectable()
export class UserService {
  public userIsUpdated: Subject<User>;

  private user: User;
  private userApiUrl: string;

  constructor(private http: Http) {
    this.userApiUrl = `${environment.host}/api/user`;
    this.userIsUpdated = new Subject<User>();
  }

  getUser(): User {
    return this.user;
  }

  getUserFromToken(): Observable<{ message: string, obj: { user: User } }> {
    const token: string = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.get(this.userApiUrl + token)
      .map((response: Response) => {
        const json = response.json();
        const user = json && json.obj && json.obj.user;
        if (user) {
          this.user = new User({
            email: user.email,
            password: user.password,
            firstName: user.firstName,
            lastName: user.lastName,
            bookmarks: user.bookmarks
          });
          this.userIsUpdated.next(this.user); // TODO: No setUser call?
        }
        return response.json();
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  setUser(user: User): void {
    this.user = user;
    this.userIsUpdated.next(this.user); // TODO: Check the necessity of this.
  }

}
