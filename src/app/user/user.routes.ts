import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth/auth.component';
import { BookmarkListComponent } from './bookmarks/bookmark-list/bookmark-list.component';

const userRoutes: Routes = [
  {path: '', redirectTo: 'bookmarks', pathMatch: 'full'},
  {path: 'bookmarks', component: BookmarkListComponent},
  {path: 'login', component: AuthComponent},
  {path: 'register', component: AuthComponent},
  {path: '**', redirectTo: 'bookmarks'}
];

export const userRouting = RouterModule.forChild(userRoutes);
