import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthComponent } from './auth/auth.component';
import { BookmarkListComponent } from './bookmarks/bookmark-list/bookmark-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { userRouting } from './user.routes';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    userRouting
  ],
  declarations: [
    AuthComponent,
    BookmarkListComponent
  ]
})
export class UserModule {}
