import { Item } from '../../shared/items/model/item.model';
import { isNullOrUndefined } from 'util';

import { Bookmark } from '../bookmarks/model/bookmark.model';

export class User {
  _id: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  bookmarks: Bookmark[];

  constructor(user: {
    _id?: number,
    email: string,
    password: string,
    firstName?: string,
    lastName?: string,
    bookmarks?: Bookmark[]
  }) {
    for (const property in user) {
      if (!isNullOrUndefined(user[property])) {
        if (property === 'bookmarks') {
          const bookmarks = [];
          for (const bookmark of user.bookmarks) {
            bookmarks.push(
              new Bookmark(new Item(bookmark.item), bookmark.creationDate, bookmark._id)
            );
          }
          this.bookmarks = bookmarks;
        } else {
          this[property] = user[property];
        }
      }
    }
  }

}
