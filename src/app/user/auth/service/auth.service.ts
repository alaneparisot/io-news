import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../../environments/environment';
import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';


@Injectable()
export class AuthService {
  private userApiUrl = `${environment.host}/api/user`;

  constructor(private http: Http,
              private router: Router,
              private userService: UserService) { }

  isAuthenticated() {
    return localStorage.getItem('token') !== null;
  }

  login(user: User) {
    return this.authenticate(user, 'login');
  }

  logout(goToLogin?: boolean) {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    this.userService.setUser(null);
    if (goToLogin) {
      this.router.navigateByUrl('user/login');
    } else {
      this.router.navigateByUrl('/');
    }
  }

  register(user: User) {
    return this.authenticate(user, 'register');
  }

  // Private Methods -----------------------------------------------------------

  private authenticate(user: User, mode: string) {
    const body = JSON.stringify(user);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(`${this.userApiUrl}/${mode}`, body, {headers: headers})
      .map((response: Response) => {
        const res = response.json();
        localStorage.setItem('token', res.token);
        localStorage.setItem('userId', res.user._id);
        this.userService.setUser(new User(res.user));
        return res;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

}
