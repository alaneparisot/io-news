import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {
  HttpModule,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { UserService } from '../../service/user.service';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { User } from '../../model/user.model';

class MockRouter {
  navigateByUrl(url: string) { return url; }
}

describe('AuthService', () => {
  let service: AuthService;
  let userService: UserService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpModule
      ],
      providers: [
        AuthService,
        UserService,
        {provide: XHRBackend, useClass: MockBackend},
        {provide: Router, useClass: MockRouter}
      ]
    });
  });

  beforeEach(inject(
    [AuthService, UserService, Router],
    (authService: AuthService, _userService: UserService, _router: Router) => {
      service = authService;
      userService = _userService;
      router = _router;
    })
  );

  it('should be created', () => {
    expect(service).toBeDefined();
  });


  describe('isAuthenticated', () => {

    it('should return true', () => {
      window.localStorage.setItem('token', 'token-test-1');
      expect(service.isAuthenticated()).toBeTruthy();
    });

    it('should return false', () => {
      window.localStorage.removeItem('token');
      expect(service.isAuthenticated()).toBeFalsy();
    });

  });


  describe('authenticate', () => {

    const test = (should: string, mode: string, token: string, userId: number) => {
      it(should, inject([XHRBackend], (mockBackend: MockBackend) => {
          mockBackend.connections.subscribe((connection: MockConnection) => {
            expect(connection.request.url).toMatch(new RegExp(`${mode}$`));
            connection.mockRespond(new Response(new ResponseOptions({
              body: JSON.stringify(mockResponse)
            })));
          });

          const user = new User({
            _id: userId,
            email: 'john@doe.com',
            password: 'secret'
          });

          const mockResponse = {
            token: token,
            user: user
          };

          spyOn(userService, 'setUser');

          service[mode](user).subscribe(() => {
            expect(window.localStorage.getItem('token')).toBe(token);
            expect(window.localStorage.getItem('userId')).toBe(userId.toString());
            expect(userService.setUser).toHaveBeenCalledWith(jasmine.any(User));
          });
        })
      );
    };

    test('should log user in', 'login', 'token-login', 123);
    test('should register user', 'register', 'token-register', 456);

  });


  describe('logout', () => {

    beforeEach(() => {
      spyOn(router, 'navigateByUrl');
    });

    it('should log user out', () => {
      window.localStorage.setItem('token', 'token-logout');
      window.localStorage.setItem('userId', '789');

      spyOn(userService, 'setUser');

      service.logout();

      expect(window.localStorage.getItem('token')).toBeNull();
      expect(window.localStorage.getItem('userId')).toBeNull();
      expect(userService.setUser).toHaveBeenCalledWith(null);
      expect(router.navigateByUrl).toHaveBeenCalledWith('/');
    });

    it('should got to login page', () => {
      service.logout(true);
      expect(router.navigateByUrl).toHaveBeenCalledWith('user/login');
    });

  });

});
