import {
  async, ComponentFixture, fakeAsync, inject,
  TestBed, tick
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AuthComponent } from './auth.component';
import { AuthService } from './service/auth.service';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { User } from '../model/user.model';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  let authService: AuthService;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [RouterTestingModule.withRoutes([
          {path: 'login', component: AuthComponent},
          {path: 'register', component: AuthComponent},
        ]), HttpModule, ReactiveFormsModule],
        declarations: [AuthComponent],
        providers: [AuthService, UserService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;

    authService = fixture.debugElement.injector.get(AuthService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });


  describe('ngOnInit', () => {

    it('should create form', () => {
      expect(component.form).toBeDefined();
    });

  });


  describe('isRegister', () => {

    let router: Router;

    beforeEach(inject([Router], (_router: Router) => {
      router = _router;
    }));

    it('should return true', fakeAsync(() => {
      router.navigateByUrl('register');
      tick();
      expect(component.isRegister()).toBeTruthy();
    }));

    it('should return false', fakeAsync(() => {
      router.navigateByUrl('login');
      tick();
      expect(component.isRegister()).toBeFalsy();
    }));

  });


  describe('onSubmit', () => {

    let router: Router;

    beforeEach(inject([Router], (_router: Router) => {
      router = _router;

      spyOn(component.form, 'reset');
    }));

    it('should log user in', () => {
      spyOn(router, 'navigateByUrl');
      spyOn(component, 'isRegister').and.returnValue(false);
      spyOn(authService, 'login').and.returnValue(Observable.of({}));

      component.onSubmit();

      expect(authService.login).toHaveBeenCalledWith(jasmine.any(User));
      expect(router.navigateByUrl).toHaveBeenCalledWith('user/bookmarks');
      expect(component.form.reset).toHaveBeenCalled();
    });

    it('should register user', () => {
      spyOn(component, 'isRegister').and.returnValue(true);
      spyOn(authService, 'register').and.returnValue(Observable.of({}));

      component.onSubmit();

      expect(authService.register).toHaveBeenCalledWith(jasmine.any(User));
      expect(component.form.reset).toHaveBeenCalled();
    });

  });
});
