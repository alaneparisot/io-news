import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import 'rxjs/add/operator/filter';

import { User } from '../model/user.model';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    const validEmailRegex = new RegExp(`^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>
    ()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}
    \\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$`);

    // TODO: Find if a better way exists
    // Look for a better way to handle the fact
    // that both register and login use the same component.
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(
        (event: NavigationEnd) => {
          this.initForm(this.isRegister(), validEmailRegex);
        }
      );

    this.initForm(this.isRegister(), validEmailRegex);
  }


  isRegister() {
    return this.router.url.indexOf('register') !== -1;
  }


  onSubmit() {
    // TODO: use this.form.value as a single arg?
    const user = new User({
      email: this.form.value.email,
      password: this.form.value.password,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName
    });

    if (this.isRegister()) {
      // Register
      this.authService.register(user)
        .subscribe(
          data => this.router.navigateByUrl('/'),
          // error => console.log('register error', error) // TODO
        );
    } else {
      // Login
      this.authService.login(user)
        .subscribe(
          data => this.router.navigateByUrl('user/bookmarks'),
          // error => console.log('login error', error) // TODO
        );
    }

    this.form.reset();
  }


  // Private Methods -----------------------------------------------------------

  private initForm(isRegister: boolean, validEmailRegex: RegExp) {
    const formControls = {
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(validEmailRegex)
      ]),
      password: new FormControl(null, Validators.required)
    };

    if (isRegister) {
      Object.assign(formControls, {
        firstName: new FormControl(null, Validators.required),
        lastName: new FormControl(null, Validators.required)
      });
    }

    this.form = new FormGroup(formControls);
  }

}
