import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';
import { Bookmark } from '../model/bookmark.model';
import { ItemService } from '../../../shared/items/services/item/item.service';
import { BookmarkService } from '../service/bookmark.service';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.scss']
})
export class BookmarkListComponent implements OnInit, OnDestroy {
  bookmarks: Bookmark[];
  userUpdateSubscription: Subscription;

  constructor(private bookmarkService: BookmarkService,
              private itemService: ItemService,
              private userService: UserService) { }

  ngOnInit() {

    // User got on logging in
    const user: User = this.userService.getUser();
    this.bookmarks = user && user.bookmarks;

    // User got on reloading (if valid token)
    this.userUpdateSubscription = this.userService.userIsUpdated
      .subscribe(
        (userUpdated: User) => {
          this.bookmarks = userUpdated && userUpdated.bookmarks;
        }
      );
  }

  deleteBookmark(bookmark): void {
    this.bookmarkService.deleteBookmark(bookmark._id)
      .subscribe(
        // data => console.log('deleteBookmark:success', data), // TODO
        // error => console.error('deleteBookmark:error', error) // TODO
      );
  }

  getItemHnUrl(itemId: number): string {
    return this.itemService.getItemHnUrl(itemId);
  }

  getItemWebsite(url: string, hnAsUrl: boolean): string {
    return this.itemService.getItemWebsite(url, hnAsUrl);
  }

  ngOnDestroy() {
    this.userUpdateSubscription.unsubscribe();
  }

}
