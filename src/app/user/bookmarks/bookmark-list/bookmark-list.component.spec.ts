import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { BookmarkListComponent } from './bookmark-list.component';
import { BookmarkService } from '../service/bookmark.service';
import { UserService } from '../../service/user.service';
import { ItemService } from '../../../shared/items/services/item/item.service';
import { ListService } from '../../../shared/items/services/list/list.service';
import { User } from '../../model/user.model';
import { Item } from '../../../shared/items/model/item.model';
import { Bookmark } from '../model/bookmark.model';

describe('BookmarkListComponent', () => {
  let component: BookmarkListComponent;
  let fixture: ComponentFixture<BookmarkListComponent>;

  let bookmarkService: BookmarkService;
  let itemService: ItemService;
  let userService: UserService;

  const bookmark1 = new Bookmark(new Item({}), 1, 'bookmarkId1');
  const bookmark2 = new Bookmark(new Item({}), 2, 'bookmarkId2');
  const bookmark3 = new Bookmark(new Item({}), 3, 'bookmarkId3');

  const user = new User({
    email: 'john@doe.com',
    password: 'secret',
    bookmarks: [bookmark1, bookmark2]
  });

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        imports: [HttpModule],
        declarations: [BookmarkListComponent],
        providers: [BookmarkService, ItemService, ListService, UserService]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkListComponent);
    component = fixture.componentInstance;

    bookmarkService = fixture.debugElement.injector.get(BookmarkService);
    itemService = fixture.debugElement.injector.get(ItemService);
    userService = fixture.debugElement.injector.get(UserService);
  });

  beforeEach(() => {
    spyOn(userService, 'getUser').and.returnValue(user);
    // Done here so userUpdateSubscription is defined when ngOnDestroy is called.
    // This way, there's no "Error during cleanup of component" error.
    fixture.detectChanges();
  });

  beforeEach(() => {
    spyOn(component.userUpdateSubscription, 'unsubscribe');
  });

  afterAll(() => {
    expect(component.userUpdateSubscription.unsubscribe).toHaveBeenCalled();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });


  describe('ngOnInit', () => {

    it('should set bookmarks on logging in', () => {
      expect(component.bookmarks).toBe(user.bookmarks);
    });

    it('should set bookmarks on reloading', () => {
      user.bookmarks.push(bookmark3);
      fixture.detectChanges();
      userService.userIsUpdated.next(user);
      expect(component.bookmarks).toBe(user.bookmarks);
    });

  });


  describe('deleteBookmark', () => {

    it('should call BookmarkService.deleteBookmark', () => {
      const bookmark = new Bookmark(new Item({}), 1, 'bookmarkId');
      spyOn(bookmarkService, 'deleteBookmark').and.returnValue(Observable.of());
      component.deleteBookmark(bookmark);
      expect(bookmarkService.deleteBookmark).toHaveBeenCalledWith('bookmarkId');
    });

  });


  describe('getItemHnUrl', () => {

    it('should get item HN URL', () => {
      spyOn(itemService, 'getItemHnUrl').and.returnValue('hnUrl');
      expect(component.getItemHnUrl(123)).toBe('hnUrl');
    });

  });


  describe('getItemWebsite', () => {

    it('should get item website', () => {
      spyOn(itemService, 'getItemWebsite').and.returnValue('website');
      expect(component.getItemWebsite('url', true)).toBe('website');
    });

  });

});
