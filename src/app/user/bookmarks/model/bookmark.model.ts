import { Item } from '../../../shared/items/model/item.model';

export class Bookmark {

  constructor(public item: Item,
              public creationDate?: number,
              public _id?: string) {}

}
