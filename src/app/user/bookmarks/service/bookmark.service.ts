import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../../environments/environment';
import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';
import { Bookmark } from '../model/bookmark.model';

@Injectable()
export class BookmarkService {
  private bookmarkApiUrl = `${environment.host}/api/bookmark`;

  constructor(private http: Http,
              private userService: UserService) { }

  addBookmark(itemId: number) {
    const token: string = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.post(this.bookmarkApiUrl + token, {itemId: itemId})
      .map((response: Response) => {
        const res = response.json();
        this.userService.setUser(new User(res.obj.user));
        return res;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  deleteBookmark(bookmarkId: string) { // TODO: bookmarkId is of type number?
    const token: string = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.delete(this.bookmarkApiUrl + '/' + bookmarkId + token)
      .map((response: Response) => {
        const res = response.json();
        this.userService.setUser(new User(res.obj.user));
        return res;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  deleteBookmarkFromItemId(itemId: number) {
    const user = this.userService.getUser();
    for (const bookmark of user.bookmarks) {
      if (bookmark.item._id === itemId) {
        return this.deleteBookmark(bookmark._id);
      }
    }
  }

  isBookmarked(itemId: number): boolean { // TODO: bookmarkId is of type number?
    const user = this.userService.getUser();
    if (!user) { return false; }
    for (const bookmark of user.bookmarks) {
      if (bookmark.item._id === itemId) { return true; }
    }
    return false;
  }

}
