import { TestBed, inject } from '@angular/core/testing';
import {
HttpModule,
Response,
ResponseOptions,
XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { BookmarkService } from './bookmark.service';
import { UserService } from '../../service/user.service';
import { User } from '../../model/user.model';
import { Item } from '../../../shared/items/model/item.model';
import { Bookmark } from '../model/bookmark.model';

describe('BookmarkService', () => {
  let service: BookmarkService;
  let userService: UserService;

  const user = new User({
    email: 'john@doe.com',
    password: 'secret',
    bookmarks: [
      new Bookmark(new Item({_id: 123}), 1, 'bookmardId1'),
      new Bookmark(new Item({_id: 456}), 2, 'bookmardId2'),
      new Bookmark(new Item({_id: 789}), 2, 'bookmardId3')
    ]
  });

  const mockResponse = {
    obj: {
      user: user
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        BookmarkService,
        UserService,
        {provide: XHRBackend, useClass: MockBackend}
      ]
    });
  });

  beforeEach(inject(
    [BookmarkService, UserService],
    (bookmarkService: BookmarkService, _userService: UserService) => {
      service = bookmarkService;
      userService = _userService;
    })
  );

  it('should be created', () => {
    expect(service).toBeDefined();
  });


  describe('addBookmark + deleteBookmark', () => {

    const test = (should: string,
                  mode: string,
                  id: (number | string),
                  urlSuffix: string = '') => {
      const url = `api/bookmark${urlSuffix}\\?token=token-bookmark$`;

      it(should, inject([XHRBackend], (mockBackend: MockBackend) => {
        window.localStorage.setItem('token', 'token-bookmark');

        mockBackend.connections.subscribe((connection) => {
          expect(connection.request.url).toMatch(new RegExp(url));
          connection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(mockResponse)
          })));
        });

        spyOn(userService, 'setUser');

        service[`${mode}Bookmark`](id).subscribe(() => {
          expect(userService.setUser).toHaveBeenCalledWith(jasmine.any(User));
        });
      }));
    };

    test('should add bookmark', 'add', 123);
    test('should delete bookmark', 'delete', 'bookmarkId', '/bookmarkId');

  });


  describe('deleteBookmarkFromItemId', () => {

    it('should delete bookmark from item ID', () => {
      spyOn(userService, 'getUser').and.returnValue(user);
      spyOn(service, 'deleteBookmark');
      service.deleteBookmarkFromItemId(456);
      expect(service.deleteBookmark).toHaveBeenCalledWith('bookmardId2');
    });

  });


  describe('isBookmarked', () => {

    it('should return true', () => {
      spyOn(userService, 'getUser').and.returnValue(user);
      expect(service.isBookmarked(456)).toBeTruthy();
    });

    it('should return false', () => {
      spyOn(userService, 'getUser').and.returnValue(user);
      expect(service.isBookmarked(147)).toBeFalsy();
    });

    it('should return false (no user)', () => {
      spyOn(userService, 'getUser').and.returnValue(null);
      expect(service.isBookmarked(456)).toBeFalsy();
    });

  });

});
