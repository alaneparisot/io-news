const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const mongoose = require('mongoose');

const config = require('./server.config');

const Lists = require('./server/functions/lists');
const hnLists = require('./server/constants/hnLists');

const app = express();

mongoose.connect(`mongodb://${config.mongodb.url}`, {
  useMongoClient: true
});

// API files for interacting with MongoDB
const listApi = require('./server/routes/list');
const userApi = require('./server/routes/user');
const bookmarkApi = require('./server/routes/bookmark');

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// APIs location
app.use('/api/list', listApi);
app.use('/api/user', userApi);
app.use('/api/bookmark', bookmarkApi);

// Send all other requests to the Angular app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));

// The Real Stuff

Lists.watch(hnLists);
