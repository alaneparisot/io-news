# io News 0.2.3

io News is a news aggregator based on the [Hacker News API](https://github.com/HackerNews/API).

As a frontend developer, working with [AngularJS](https://angularjs.org),
I started this side project to get a better grip on [Angular](https://angular.io/),
[Node.js](https://nodejs.org), and [MongoDB](https://www.mongodb.com/).

### [Demo](https://io-news.herokuapp.com/)


## Contact

If you have any suggestions or questions, please contact me at myFirstName.myLastName@gmail.com

I speak French, English, and a bit of German.


## Changelog

### 0.2.3

* Fix duplication in Readme

### 0.2.2

* Design refresh

### 0.2.1

* Fix bugs on mobile display

### 0.2.0

* User authentication
* Bookmark management
* Lazy loading on the User module
* Add tooltips
* Add Google Analytics
* Add changelog
* Unit testing code coverage > 98%

### 0.1.5

* Redesign the way lists are selected

### 0.1.4

* Add frontend unit tests

### 0.1.3

* Add spinner
* Improve display on mobile phones
* Add job label

### 0.1.2

* Fix backend error

### 0.1.1

* Add thin story row borders when lists are sorted

### 0.1.0

* Full stack development: Angular + Node.js + mongoDB
* Lists load faster, API calls are drastically reduced
* Preferences are saved in the local storage
* Add custom favicon
* Hosted on Heroku and mLab

### 0.0.4

* Add list sorting by score and comments
* Display story's website (domain name)

### 0.0.3

* Review style, especially for mobile phones

### 0.0.2

* Refactor internal architecture

### 0.0.1

* Frontend development only, in Angular
* Display lists of stories in a table
* Color story rows accordingly to their score
* No backend, just Hacker News API calls
* Hosted on Firebase Hosting
